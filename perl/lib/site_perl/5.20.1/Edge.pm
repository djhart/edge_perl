package Edge;
use vars qw(@ISA @EXPORT);
use strict;
use Data::Dumper;
use DBI;
use SOAP::Lite;
use HTTP::Cookies;
use LWP::Simple;
use IO::Socket::SSL qw( SSL_VERIFY_NONE );
use Getopt::Long;
use Try::Tiny;
use utf8;
$Data::Dumper::Indent = 1;
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(getSoap getDBH getLog getTypeId getNextId getCollectionTypeId removeOrphans addDevice addReport addReportAttachment getDevIdFromAppDevId addAlert getPrimaryKeys getEdgeKeys processCollections addReportTemplate addUser getDomainIdFromDomainName addMapping getAppColId addStatus GetOptions);

sub addMapping{
  my $dbh            = $_[0];
  my (
    $table,
    $keys,
    $values ) = split('\|',$_[1]);

  my ($key_1  ,$key_2)   = split(',',$keys);
  my ($value_1,$value_2) = split(',',$values);

  my $statement = qq|insert into $table ($keys) values (?,?) ON DUPLICATE KEY UPDATE $key_1 = $value_1,$key_2 = $value_2|;

  my $dbst      = qq|insert into $table ($keys) values ($value_1,$value_2) ON DUPLICATE KEY UPDATE $key_1 = $value_1,$key_2 = $value_2|;

  print "dbst:$dbst\n";

  my $sth = $dbh->prepare($statement);
  try{
    $sth->execute($value_1,$value_2);
  }
}


sub processCollections{
  my $dbh            = $_[0];
  my $application    = $_[1];
  my $ref_entries    = $_[2];
  my $collection_id  = getNextId($dbh,'collection');
  my $application_id = getTypeId($dbh,'application',$application);
  my %COL            = ();

  print "DBG:inside processCollections\n";

  foreach my $item (@$ref_entries){
    next if ($item =~ m/^NOW/);
    print "DBG:item:$item\n";

    my $uniq = "";

    my ($collection_type,$app_collection_id,$name,$parent_id) = split('\|',$item);
    my $collection_type_id = getTypeId($dbh,'collection_type',$collection_type);

    $uniq = $app_collection_id . $collection_type_id . $application_id;
    $COL{$uniq} = "";


    my $statement = qq|insert into collection (collection_id,name,app_collection_id,parent_id,application_id,collection_type_id) values (?,?,?,?,?,?) ON DUPLICATE KEY UPDATE collection_type_id=$collection_type_id|;

    print "DBG:statement:$statement\n";

    my $sth = $dbh->prepare($statement);

    #try{ #TODO find out why this isn't working
      $sth->execute($collection_id,$name,$app_collection_id,$parent_id,$application_id,$collection_type_id);
    #}

  $collection_id++;
  }
  return \%COL;
}

sub getPrimaryKeys{
  my $dbh  = $_[0];
  my ($db) = $dbh->selectrow_array("select DATABASE()");  

  my $dbst     = qq|SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND table_schema='$db' ORDER BY table_name ASC|;
  my $arrayref = $dbh->selectall_arrayref($dbst);
  my @tables   = @$arrayref;

  my %EDGE_KEYS;

  foreach my $item (@tables){
    my $table    = @$item[0];
    my $dbst     = qq|show create table $table|;
    my $arrayref = $dbh->selectall_arrayref($dbst);

    my $key;
    foreach my $entry (@{$arrayref}){
      my $result = join('|',@{$entry});
      ($key)     = $result =~ m/PRIMARY KEY \((.*)\)/;
      $key       =~ s/\`//g;
    }
  $EDGE_KEYS{$table} = $key;
  }
  return \%EDGE_KEYS;
}

sub getEdgeKeys{
  my $dbh = $_[0];
  my ($application,$table,$keys ) = split('\|',$_[1]);
  my $application_id = getTypeId($dbh,'application',$application);
  my %EDGE_KEYS = ();
  my $table_id;
  my $dbst;

  if ($table =~ m/\_to\_/){
    $dbst     = qq|select $keys from $table|;

  } else {
    $table_id = $table . "_id";
    $dbst     = qq|select $table_id,$keys from $table where application_id = $application_id|;
  }

  #TODO this will not work for mappings like location_to_collection.
  #TODO need to add application_id to all mapping tables
  #TODO need to have dbst below that doesn't get $table_id for these

  #TODO CHECK THIS OUT.  Make sure it still works!

  my $arrayref = $dbh->selectall_arrayref($dbst);
#  print Dumper($arrayref);
  print "dbst=$dbst\n";

  foreach my $entry (@{$arrayref}){
    my $fields = join('|',@{$entry});
    #print "fields=$fields\n";
    my ($device_id,$app_device_id,$application_id,$device_type_id) = split('\|',$fields);  #TODO why do all of these have device_id ? :)
    my $uniq = $app_device_id . $application_id . $device_type_id;
    $EDGE_KEYS{$uniq} = $device_id;
  }
  return \%EDGE_KEYS;
}


sub addAlert{
  my $dbh = $_[0];
  my (
    $alert_id,
    $app_device_id,
    $app_alert_id,
    $device_id,
    $application_id,
    $acknowledged_id,
    $assigned_to_id,
    $end_time_epoch,
    $message,
    $count,
    $origin,
    $app_severity,
    $edge_severity,
    $start_time_epoch) = split('\|',$_[1]);

  my $statement = qq|
    insert into alert (
      alert_id,
      app_device_id,
      app_alert_id,
      device_id,
      application_id,
      acknowledged_id,
      assigned_to_id,
      end_time_epoch,
      message,
      count,
      origin,
      app_severity,
      edge_severity,
      start_time_epoch
    ) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?) 

      ON DUPLICATE KEY UPDATE
        app_severity    = $app_severity,
        edge_severity   = $edge_severity,
        acknowledged_id = $acknowledged_id,
        message         = "$message",
        assigned_to_id  = $assigned_to_id,
        count           = $count
  |;

  my $sth = $dbh->prepare($statement);
  try{
    $sth->execute(
      $alert_id,
      $app_device_id,
      $app_alert_id,
      $device_id,
      $application_id,
      $acknowledged_id,
      $assigned_to_id,
      $end_time_epoch,
      $message,
      $count,
      $origin,
      $app_severity,
      $edge_severity,
      $start_time_epoch);
  }
}

sub addReport{
  my $dbh = $_[0];
  my (
    $report_id,
    $name,
    $description,
    $user_id,
    $is_private,
    $frequency_unit,
    $next_run,
    $timezone) = split('\|',$_[1]);

  my $statement = qq|
    insert into report (report_id,
      name,
      description,
      user_id,
      is_private,
      frequency_unit,
      next_run,
      timezone) values (?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE name = '$name'|;

  my $sth = $dbh->prepare($statement);
  try{
    $sth->execute(
      $report_id,
      $name,
      $description,
      $user_id,
      $is_private,
      $frequency_unit,
      $next_run,
      $timezone);
  }
}

sub addReportTemplate{
  my $dbh = $_[0];
  my (
    $report_template_id,
    $name,
    $is_custom,
    $app_id ) = split('\|',$_[1]);

  my $statement = qq|
    insert into report_template (
      report_template_id,
      name,
      is_custom,
      app_id ) values (?,?,?,?) ON DUPLICATE KEY UPDATE name = '$name'|;

  my $sth = $dbh->prepare($statement);
  try{
    $sth->execute( $report_template_id, $name, $is_custom, $app_id );
  }
}

sub getDomainIdFromDomainName{
  my $dbh            = $_[0];
  my $match          = $_[1];
  my $arrayref = $dbh->selectcol_arrayref( qq|select domain_id from domain where name = '$match'|);
  my $id       = getInt($arrayref);
  return $id;
}

sub getDevIdFromAppDevId{
  my $dbh            = $_[0];
  my $match          = $_[1];
  my $application_id = $_[2];
  my $arrayref = $dbh->selectcol_arrayref( qq|select device_id from device where app_device_id = $match and application_id = $application_id|);
  my $id       = getInt($arrayref);
  return $id;
}

sub getAppColId{
  my $dbh            = $_[0];
  my $match          = $_[1];
  my $arrayref = $dbh->selectcol_arrayref( qq|select app_collection_id from collection where name = '$match'|);
  my $id       = getInt($arrayref);
  return $id;
}

sub addReportAttachment{
  my $dbh = $_[0];
  my (
    $report_attachment_id,
    $report_id,
    $name,
    $type) = split('\|',$_[1]);

  my $statement = qq|
    insert into report_attachment (
      report_attachment_id,
      report_id,
      name,
      type) values (?, ?, ?, ?) ON DUPLICATE KEY UPDATE name = '$name'|;

  my $sth = $dbh->prepare($statement);
  try{
    $sth->execute( $report_attachment_id, $report_id, $name, $type);
  }
}

sub addUser{
  my $dbh = $_[0];
  my (
    $user_id,
    $app_user_id,
    $application_id,
    $username,
    $first,
    $last,
    $description) = split('\|',$_[1]);

  my $statement = qq|
    insert into user (
      user_id,
      app_user_id,
      application_id,
      username,
      first,
      last,
      description)values (?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE first = '$first', last = '$last', description = '$description'|;

  my $sth = $dbh->prepare($statement);
  try{
    $sth->execute($user_id, $app_user_id, $application_id, $username, $first, $last, $description);
  }
}

sub addDevice{
  my $dbh = $_[0];
  my (
    $device_id,
    $app_device_id,
    $application_id,
    $dns_name,
    $name,
    $alias,
    $ip_address,
    $description,
    $device_type_id) = split('\|',$_[1]);

  my $statement = qq|
    insert into device (
      device_id,
      app_device_id,
      application_id,
      dns_name,
      name,
      alias,
      ip_address,
      description,
      device_type_id) values (?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE name = '$name'|;

  my $dbst      = qq|insert into device (device_id,app_device_id,application_id,dns_name,name,alias,ip_address,description,device_type_id) 
                  values ($device_id,$app_device_id,$application_id,$dns_name,$name,$alias,$ip_address,$description,$device_type_id)|;
  my $sth = $dbh->prepare($statement);
  try{
    $sth->execute($device_id,$app_device_id,$application_id,$dns_name,$name,$alias,$ip_address,$description,$device_type_id);
  }
}

sub addStatus{
  my $dbh = $_[0];
  my (
    $table_name,
    $table_entity_id,
    $status_type_id,
    $scope_id,
    $severity) = split('\|',$_[1]);

print "DBG: @_\n";

  my $statement = qq|
    insert into status (
    table_name,
    table_entity_id,
    status_type_id,
    scope_id,
    severity) 
      values (?,?,?,?,?) ON DUPLICATE KEY UPDATE severity = $severity|;

  my $sth = $dbh->prepare($statement);
  try{
    $sth->execute($table_name, $table_entity_id, $status_type_id, $scope_id, $severity);
  }
}



#
# This will compare what is currently configured in the remote app (SevOne)
# with what is currently in the Edge db after an update.
# Each list should be a name/value pair of uniq entries ( concat'd primary keys )
# from the Edge table.  Anything in the Edge table that is not in the app should be 
# deleted from the Edge db.

sub removeOrphans{
  my ($dbh,$table,$app,$edge) = @_;

  my $table_id = $table . "_id";

  foreach my $key (keys %{$app}){

#    print "DBG:\n key:$key\nedge:$$edge{$key}\n\n";
    if (exists $$edge{$key}){
      delete($$edge{$key});
    }
  }

  my $orphan_count = keys %$edge;
  print "$orphan_count : left in edge\n";
#  print Dumper($edge);

  foreach my $key (keys %{$edge}){
    my $id = $$edge{$key};

    my $dbst = qq|delete from $table where $table_id = $id|;

    try{
      $dbh->do($dbst);
    }
  }
}

sub getInt{
  my $ref = $_[0];
  my @num      = @{$ref};
  my $id       = @num[0];
  return $id;
}

sub getTypeId{
  my ($dbh,$type,$name) = @_;

  my $id         = $type . "_id";

  my $dbst       = qq|select $id from $type where name = '$name'|;
  my $arrayref   = $dbh->selectcol_arrayref($dbst);
  my $id         = getInt($arrayref);
  return $id;
}

sub getNextId{
  my $dbh            = $_[0];
  my $table_name     = $_[1];
  my $application_id = $_[2]; # Optional.  If present, get app id from table

  my $id;
  my $dbst;

  if ($application_id){
    $id   = "app_" . $table_name . "_id";
    $dbst = qq|select coalesce(MAX($id),1) from $table_name where application_id = $application_id|;
  } else {
    $id   = $table_name . "_id";
    $dbst = qq|select coalesce(MAX($id),1) from $table_name where $id < 50000000|;
  }

  my $arrayref   = $dbh->selectcol_arrayref($dbst);
  my $id         = getInt($arrayref);
  return $id;
}

sub getSoap{
  my $user       = $_[0];
  my $pass       = $_[1];
  my $url        = $_[2];

  my $cookie_jar = HTTP::Cookies->new();
  my $soap       = SOAP::Lite
        -> uri( 'http://www.sevone.com/' )
        -> proxy( "$url/soap3/api.php", cookie_jar => $cookie_jar )
        -> on_fault( sub { my($soap, $res) = @_; die ref $res ? $res->faultstring : $soap->transport->status, "\n"; });

  # if set to https, it's unlikely we'll have a CA cert for it...
  if ( $url =~ m/^https/ ) {
       $soap->transport->ssl_opts(
       verify_hostname => 0,
       SSL_verify_mode => SSL_VERIFY_NONE
    );
  }

  # attempt to login to SevOne - if successful, a session cookie is set
  my $result = $soap->authenticate($user,$pass)->result;
  if ( $result == 0 ) {
    print STDERR "fatal error authenticating to SevOne server $url for user $user.\n";
    exit 1;
  }
  return $soap;
}

sub getLog{
  my $logname  = $_[0];
  my $EDGE_HOME="/opt/edge/server"; #TODO make this an argument
  $logname     =~ s/\.pl//;  # strip off the .pl
  my $LogFile  = "$EDGE_HOME/logs/$logname" . ".log";

  my $LogLevel = 5;
  my $log = new Log::LogLite($LogFile,$LogLevel);
  $log->template( "<date> <default_message><message>\n" );
  $log->write( "$logname running", 2);
  return $log;
}

sub getDBH{
  my ($db_host, $db_port, $db_name, $db_user, $db_pass ) = split('\|',$_[0]);
  my $silent = $_[1];

  my $dbh = DBI->connect("DBI:mysql:database=$db_name;host=$db_host;port=$db_port", $db_user, $db_pass);
  
  if ($silent){
    print "silent was requested\n";

    $dbh->{RaiseError} = 0;
    $dbh->{PrintError} = 0;
  } else {
    $dbh->{RaiseError} = 1;
    $dbh->{PrintError} = 1;
  }

  #Needed for handling spanish characters
  $dbh->{'mysql_enable_utf8'} = 1;
  $dbh->do('set names "utf8"');
  #$dbh->do('set session collation_connection=utf8_spanish_ci');

  return $dbh;
}
1;
